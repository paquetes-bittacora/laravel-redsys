<?php

declare(strict_types=1);

namespace Bittacora\Laravel\Redsys\Commands;

use Bittacora\Laravel\Redsys\Services\ConfigFileUpdater;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Ssheduardo\Redsys\RedsysServiceProvider;

final class InstallCommand extends Command
{
    private const PUBLISHED_CONFIG_FILE_PATH = 'config/redsys.php';
    /** @var string  */
    protected $signature = 'laravel-redsys:install';

    /** @var string  */
    protected $description = 'Instala el módulo de pagos por tarjeta (Redsys)';

    public function handle(ConfigFileUpdater $configFileUpdater): void
    {
        $this->comment('Publicando archivo de configuración de redsys a config/redsys.php');
        Artisan::call('vendor:publish', ['--provider' => RedsysServiceProvider::class]);

        $this->addRedsysConfigToGitignore();
        $configFileUpdater->addAdditionalFieldsToConfig();
    }

    private function addRedsysConfigToGitignore(): void
    {
        $this->comment('Añadiendo la configuración de Redsys a .gitignore');
        $fileName = base_path() . '/.gitignore';
        if (!\Illuminate\Support\Str::contains(file_get_contents($fileName), self::PUBLISHED_CONFIG_FILE_PATH)) {
            file_put_contents($fileName, self::PUBLISHED_CONFIG_FILE_PATH, FILE_APPEND);
        }
    }
}
