<?php

declare(strict_types=1);

namespace Bittacora\Laravel\Redsys\Dtos;

/**
 * Dto que indica el resultado de la notificación del TPV
 */
final class TpvNotificationResult
{
    /**
     * @param bool $success Indica si el pago fue correcto o no
     * @param int $dsResponse Código drResponse de la respuesta, para uso por otros módulos si es necesario
     * @param int|string $dsOrder El código del pedido como string (para respetar los posibles formatos del TPV)
     */
    public function __construct(
        public readonly bool $success,
        public readonly int $dsResponse,
        public readonly int|string $dsOrder,
    ) {
    }
}
