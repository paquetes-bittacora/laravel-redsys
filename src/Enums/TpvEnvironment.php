<?php

declare(strict_types=1);

namespace Bittacora\Laravel\Redsys\Enums;

enum TpvEnvironment: string
{
    case TEST = 'test';
    case PRODUCTION = 'live';
}
