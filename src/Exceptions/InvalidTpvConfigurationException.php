<?php

declare(strict_types=1);

namespace Bittacora\Laravel\Redsys\Exceptions;

use Exception;

final class InvalidTpvConfigurationException extends Exception
{
    /** @var string  */
    protected $message = "No se ha configurado el módulo de pago por tarjeta. Si quieres ignorar
            este aviso, añade 'allow_empty_configuration' => true al archivo de configuración del TPV.";
}
