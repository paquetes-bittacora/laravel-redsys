<?php

declare(strict_types=1);

namespace Bittacora\Laravel\Redsys;

use Bittacora\Laravel\Redsys\Commands\InstallCommand;
use Illuminate\Support\ServiceProvider;

final class LaravelRedsysServiceProvider extends ServiceProvider
{
    public const PACKAGE_PREFIX = 'bpanel4-redsys';

    public function boot(): void
    {
        $this->commands(InstallCommand::class);
    }
}
