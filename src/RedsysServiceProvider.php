<?php

namespace Ssheduardo\Redsys;

use Illuminate\Support\ServiceProvider;
use Sermepa\Tpv\Tpv;

class RedsysServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(): void
    {
        // Publish config files
        $this->publishes([__DIR__ . '/config/config.php' => config_path('redsys.php'),], 'config');
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind('tpv', function () {
            return new Tpv();
        });
    }
}
