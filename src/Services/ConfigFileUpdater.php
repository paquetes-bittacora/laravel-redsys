<?php

declare(strict_types=1);

namespace Bittacora\Laravel\Redsys\Services;

final class ConfigFileUpdater
{
    private const PUBLISHED_CONFIG_FILE_PATH = 'config/redsys.php';

    public function __construct(private ?string $basePath = '')
    {
        $this->basePath = '' != $this->basePath ? $basePath : base_path();
    }

    public function addAdditionalFieldsToConfig(): void
    {
        $configFilePath = $this->basePath . DIRECTORY_SEPARATOR . self::PUBLISHED_CONFIG_FILE_PATH;
        $originalFileContents = file_get_contents($configFilePath);

        if (str_contains($originalFileContents, 'REDSYS_TITULAR')) {
            return;
        }

        $search = "'tradename'             => env('REDSYS_TRADENAME',''),";
        $titular = "    'titular'               => env('REDSYS_TITULAR',''),";
        $currency = "    'currency'              => env('REDSYS_CURRENCY','978'),";
        $transactionType = "    'transaction_type'      => env('REDSYS_TRANSACTION_TYPE','0'),";
        $content = str_replace(
            $search,
            $search . "\n" . $titular . "\n" . $currency . "\n" . $transactionType,
            $originalFileContents
        );
        file_put_contents($configFilePath, $content);
    }
}
