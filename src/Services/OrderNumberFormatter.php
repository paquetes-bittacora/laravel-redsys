<?php

declare(strict_types=1);

namespace Bittacora\Laravel\Redsys\Services;

final class OrderNumberFormatter
{
    public static function formatOrderNumber(int $orderId): string
    {
        return str_pad((string )$orderId, 6, '0', STR_PAD_LEFT);
    }

    public static function fromTpvFormat(string $order): int
    {
        return (int) ltrim($order, '0');
    }
}
