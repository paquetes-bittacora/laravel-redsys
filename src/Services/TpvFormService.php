<?php

declare(strict_types=1);

namespace Bittacora\Laravel\Redsys\Services;

use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Laravel\Redsys\Enums\TpvEnvironment;
use Bittacora\Laravel\Redsys\Exceptions\InvalidTpvConfigurationException;
use Bittacora\Laravel\Redsys\Validation\ConfigValidator;
use Exception;
use Illuminate\Contracts\Config\Repository;
use Sermepa\Tpv\Tpv;

final class TpvFormService
{
    /**
     * @throws InvalidTpvConfigurationException
     */
    public function __construct(
        private readonly Tpv $tpv,
        private readonly Repository $config,
        ConfigValidator $configValidator
    ) {
        $configValidator->validatePackageConfiguration();
    }

    /**
     * @param int $amount
     * @param string $orderIdentifier Al menos 4 dígitos, máximo 12, y los 4 primeros tienen que ser numéricos (campo
     *  DS_MERCHANT_ORDER de Redsys)
     * @param string $description campo DS_MERCHANT_PRODUCTDESCRIPTION de Redsys
     * @param string $method T para pago con tarjeta, z (minúscula) para bizum
     * @return string
     */
    public function renderTpvForm(
        int $amount,
        string $orderIdentifier,
        string $description,
        string $method = 'T',
    ): string {
        try {
            $key = $this->config->get('redsys.key');

            $this->tpv->setAmount(Price::fromInt($amount)->toFloat());
            $this->tpv->setOrder($orderIdentifier);
            $this->tpv->setMerchantcode($this->config->get('redsys.merchantcode')); //Reemplazar por el código que proporciona el banco
            /** @phpstan-ignore-next-line Ignoro el error porque no sabe el tipo de retorno pero es válido */
            $this->tpv->setCurrency((int) $this->config->get('redsys.currency'));
            /** @phpstan-ignore-next-line  */
            $this->tpv->setTransactiontype((int) $this->config->get('redsys.transaction_type'));
            /** @phpstan-ignore-next-line */
            $this->tpv->setTerminal((int) $this->config->get('redsys.terminal'));
            $this->tpv->setMethod($method); //Solo pago con tarjeta, no mostramos iupay
            $this->tpv->setNotification($this->config->get('redsys.url_notification')); //Url de notificacion
            $this->tpv->setUrlOk($this->config->get('redsys.url_ok')); //Url OK
            $this->tpv->setUrlKo($this->config->get('redsys.url_ko')); //Url KO
            $this->tpv->setVersion('HMAC_SHA256_V1');
            $this->tpv->setTradeName($this->config->get('redsys.tradename'));
            $this->tpv->setTitular($this->config->get('redsys.titular'));
            $this->tpv->setProductDescription($description);
            // ¡OJO! 'enviroment' está mal escrito porque es como está en el archivo de configuración, no es una errata
            // de nuestro paquete de bPanel
            $this->tpv->setEnvironment(TpvEnvironment::from($this->config->get('redsys.enviroment'))->value); //Entorno test

            $signature = $this->tpv->generateMerchantSignature($key);
            $this->tpv->setMerchantSignature($signature);

            // Devuelve el formulario con el JS para que redirija automáticamente
            return $this->tpv->executeRedirection(true);
        } catch (Exception $e) {
            report($e);
            return $e->getMessage();
        }
    }
}
