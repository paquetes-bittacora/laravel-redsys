<?php

/** @noinspection NotOptimalIfConditionsInspection */

declare(strict_types=1);

namespace Bittacora\Laravel\Redsys\Services;

use Bittacora\Bpanel4\Orders\Enums\OrderStatus;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Laravel\Redsys\Dtos\TpvNotificationResult;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Http\Request;
use Sermepa\Tpv\Tpv;
use Sermepa\Tpv\TpvException;

final class TpvNotificationService
{
    public function __construct(
        private readonly Tpv $tpv,
        private readonly Repository $config,
    ) {
    }

    /**
     * Procesa la notificación enviada desde el TPV
     * @throws TpvException
     */
    public function processNotification(Request $request): TpvNotificationResult
    {
        $key = $this->config->get('redsys.key');
        $parameters = $this->tpv->getMerchantParameters($request->input('Ds_MerchantParameters'));
        $dsResponse = $parameters["Ds_Response"];
        $dsOrder = $parameters["Ds_Order"];

        $this->checkIfOrderHadBeenCancelled($dsOrder);

        $dsResponse += 0;
        /** @var array<string> $postData */
        $postData = $request->input();
        if ($this->tpv->check($key, $postData) && $dsResponse <= 99) {
            // El pago ha sido correcto
            return new TpvNotificationResult(true, $dsResponse, $dsOrder);
        }
        // El pago no ha sido correcto
        return new TpvNotificationResult(false, $dsResponse, $dsOrder);
    }

    /**
     * @param mixed $dsOrder
     * @return void
     */
    protected function checkIfOrderHadBeenCancelled(mixed $dsOrder): void
    {
        $orderId = OrderNumberFormatter::fromTpvFormat($dsOrder);
        $order = Order::whereId($orderId)->firstOrFail();

        if ($order->getStatus()->id === OrderStatus::CANCELLED->value) {
            abort(403, 'El pedido había sido cancelado');
        }
    }
}
