<?php

declare(strict_types=1);

namespace Bittacora\Laravel\Redsys\Validation;

use Bittacora\Laravel\Redsys\Exceptions\InvalidTpvConfigurationException;
use Illuminate\Config\Repository;
use Illuminate\Contracts\Foundation\Application;

final class ConfigValidator
{
    private readonly string $redsysConfigFilePath;

    private const CONFIG_FIELDS = [
        'redsys.key',
        'redsys.url_notification',
        'redsys.url_ok',
        'redsys.url_ko',
        'redsys.merchantcode',
        'redsys.terminal',
        'redsys.enviroment',
        'redsys.tradename',
        'redsys.titular',
        'redsys.currency',
        'redsys.transaction_type',
    ];

    public function __construct(
        private readonly Repository $config,
        private readonly Application $app,
        string $configFilePath = null,
    ) {
        $this->redsysConfigFilePath = $configFilePath ?? base_path() . '/config/redsys.php';
    }

    /**
     * @throws InvalidTpvConfigurationException
     */
    public function validatePackageConfiguration(): void
    {
        $this->validateAllFieldsArePresent();
        $this->checkIfConfigFileExists();
        $this->checkIfThereAreEmptyConfigFields();
    }

    private function emptyConfigurationIsAllowed(): bool
    {
        return !$this->app->environment('production') &&
            (true === $this->config->get('redsys.allow_empty_configuration', false));
    }

    /**
     * @throws InvalidTpvConfigurationException
     */
    private function checkIfThereAreEmptyConfigFields(): void
    {
        if ($this->emptyConfigurationIsAllowed()) {
            return;
        }

        foreach (self::CONFIG_FIELDS as $configField) {
            $this->checkIfFieldIsEmpty($configField);
        }
    }

    /**
     * @throws InvalidTpvConfigurationException
     */
    private function checkIfConfigFileExists(): void
    {
        if (!file_exists($this->redsysConfigFilePath)) {
            throw new InvalidTpvConfigurationException();
        }
    }

    /**
     * @throws InvalidTpvConfigurationException
     */
    private function validateAllFieldsArePresent(): void
    {
        foreach (self::CONFIG_FIELDS as $configField) {
            $this->checkIfConfigFieldExists($configField);
        }
    }

    /**
     * @throws InvalidTpvConfigurationException
     */
    private function checkIfFieldIsEmpty(string $configField): void
    {
        if ('' === $this->config->get($configField)) {
            throw new InvalidTpvConfigurationException();
        }
    }

    /**
     * @throws InvalidTpvConfigurationException
     */
    private function checkIfConfigFieldExists(string $configField): void
    {
        if (!$this->config->has($configField)) {
            throw new InvalidTpvConfigurationException('Falta el campo ' . $configField . ' de la ' .
                'configuración del TPV');
        }
    }
}
