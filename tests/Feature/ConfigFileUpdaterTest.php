<?php

declare(strict_types=1);

namespace Bittacora\Laravel\Redsys\Tests\Feature;

use Bittacora\Laravel\Redsys\Services\ConfigFileUpdater;
use Tests\TestCase;

final class ConfigFileUpdaterTest extends TestCase
{
    public function testAnadeElCampoTitularAlArchivoDeConfiguracion(): void
    {
        $basePath = __DIR__ . DIRECTORY_SEPARATOR . 'test-files';
        $testFileName = $basePath . '/config/redsys.php';
        $configFileUpdater = new ConfigFileUpdater($basePath);
        file_put_contents($testFileName, "'tradename'             => env('REDSYS_TRADENAME',''),");
        $configFileUpdater->addAdditionalFieldsToConfig();
        $result = file_get_contents($testFileName);
        self::assertTrue(str_contains($result, 'REDSYS_TITULAR'));
        unlink($testFileName);
    }
}
