<?php

declare(strict_types=1);

namespace Bittacora\Laravel\Redsys\Tests\Feature;

use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Laravel\Redsys\Services\TpvNotificationService;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Mockery;
use Sermepa\Tpv\Tpv;
use Sermepa\Tpv\TpvException;
use Tests\TestCase;

final class TpvNotificationServiceTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        (new OrderFactory())->createOne();
    }

    /**
     * @throws TpvException
     */
    public function testIndicaQueElPagoEsCorrectoSiElTpvDaUnaRespuestaPositiva(): void
    {
        /**
         * @var Tpv $tpvMock
         * @var Repository $configMock
         * @var Request $requestMock
         */
        [$tpvMock, $configMock, $requestMock] = $this->getMocksWithTpvResult(true);

        $tpvService = new TpvNotificationService($tpvMock, $configMock);
        $result = $tpvService->processNotification($requestMock);

        self::assertTrue($result->success);
    }

    /**
     * @throws TpvException
     */
    public function testIndicaQueElPagoEsFallidoSiElTpvDaUnaRespuestaNegativa(): void
    {
        /**
         * @var Tpv $tpvMock
         * @var Repository $configMock
         * @var Request $requestMock
         */
        [$tpvMock, $configMock, $requestMock] = $this->getMocksWithTpvResult(false);

        $tpvService = new TpvNotificationService($tpvMock, $configMock);
        $result = $tpvService->processNotification($requestMock);

        self::assertFalse($result->success);
    }

    /**
     * @return array<Tpv|Request|Repository>
     */
    private function getMocksWithTpvResult(bool $result): array
    {
        $tpvMock = Mockery::mock(Tpv::class);
        $tpvMock->shouldIgnoreMissing();
        $tpvMock->shouldReceive('getMerchantParameters')
            ->andReturn(['Ds_Response' => 0, 'Ds_Order' => '1']);
        $configMock = Mockery::mock(Repository::class);
        $configMock->shouldIgnoreMissing();
        $requestMock = Mockery::mock(Request::class);
        $requestMock->shouldReceive('input')->andReturn([]);
        $tpvMock->shouldReceive('check')->andReturn($result);
        return [$tpvMock, $configMock, $requestMock];
    }
}
