<?php

declare(strict_types=1);

namespace Bittacora\Laravel\Redsys\Tests\Unit;

use Bittacora\Laravel\Redsys\Exceptions\InvalidTpvConfigurationException;
use Bittacora\Laravel\Redsys\Validation\ConfigValidator;
use Illuminate\Config\Repository;
use Illuminate\Contracts\Foundation\Application;
use Mockery;
use PHPUnit\Framework\TestCase;

final class ConfigValidatorTest extends TestCase
{
    public function testNoLanzaExcepcionSiSePermiteIgnorarConfiguracionVacia(): void
    {
        $configRepository = $this->getConfigRepositoryMock();

        $configRepository->shouldReceive('has')->andReturnTrue();
        $configRepository->shouldReceive('get')->with('redsys.allow_empty_configuration', false)
            ->andReturnTrue();

        $configValidator = new ConfigValidator(
            $configRepository,
            $this->getAppMockWithEnvironment('testing'),
            __DIR__ . '/example-configs/empty-allowed.php',
        );

        $this->assertNull($configValidator->validatePackageConfiguration());
    }

    public function testLanzaExcepcionSiSePermiteIgnorarConfiguracionVaciaPeroEstamosEnProduccion(): void
    {
        $configRepository = $this->getConfigRepositoryMock();

        $configRepository->shouldReceive('has')->andReturnTrue();
        $configRepository->shouldReceive('get')->with('redsys.allow_empty_configuration', false)
            ->andReturnTrue();

        $configValidator = new ConfigValidator(
            $configRepository,
            $this->getAppMockWithEnvironment('production'),
            __DIR__ . '/example-configs/empty-allowed.php',
        );

        $this->assertNull($configValidator->validatePackageConfiguration());
    }

    public function testLanzaUnaExcepcionSiLaConfiguracionEstaVaciaYNoSeIndicaQueDebeIgnorarse(): void
    {
        $this->expectException(InvalidTpvConfigurationException::class);

        $configRepository = $this->getConfigRepositoryMock();

        $configValidator = new ConfigValidator(
            $configRepository,
            $this->getAppMockWithEnvironment('testing'),
            __DIR__ . '/example-configs/empty.php',
        );

        $configValidator->validatePackageConfiguration();
    }

    public function testLanzaUnaExcepcionSiNoSeHanDefinidoAlgunParametroDeConfiguracion(): void
    {
        $this->expectExceptionMessage('Falta el campo redsys.key de la configuración del TPV');

        $configRepository = $this->getConfigRepositoryMock();
        $configRepository->shouldReceive('has')->andReturnFalse();

        $configValidator = new ConfigValidator(
            $configRepository,
            $this->getAppMockWithEnvironment('testing'),
            __DIR__ . '/example-configs/empty.php',
        );

        $configValidator->validatePackageConfiguration();
    }

    public function tearDown(): void
    {
        Mockery::close();
    }

    /**
     * @return Repository&Mockery\LegacyMockInterface
     */
    private function getConfigRepositoryMock(): \Mockery\LegacyMockInterface
    {
        $configRepository = Mockery::mock(Repository::class);
        $configRepository->shouldIgnoreMissing();
        return $configRepository;
    }

    private function getAppMockWithEnvironment(string $environment = 'testing'): Application
    {
        $mock = Mockery::mock(Application::class);
        $mock->shouldReceive('environment')->andReturn($environment);
        return $mock;
    }
}
