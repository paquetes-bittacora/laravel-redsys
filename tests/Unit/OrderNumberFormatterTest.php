<?php

declare(strict_types=1);

namespace Bittacora\Laravel\Redsys\Tests\Unit;

use Bittacora\Laravel\Redsys\Services\OrderNumberFormatter;
use PHPUnit\Framework\TestCase;

final class OrderNumberFormatterTest extends TestCase
{
    /**
     * @dataProvider orderIdProvider
     */
    public function testFormateaElIdDePedidoAUnFormatoCompatibleConElTpv(string $expected, int $id): void
    {
        self::assertEquals($expected, OrderNumberFormatter::formatOrderNumber($id));
    }

    /**
     * @dataProvider orderIdProvider
     */
    public function testDevuelveElIdDePedidoAPartirDelFormatoDelTpv(string $formatted, int $expected): void
    {
        self::assertEquals($expected, OrderNumberFormatter::fromTpvFormat($formatted));
    }

    /**
     * @return array<array<string,int>>
     */
    public static function orderIdProvider(): array
    {
        return [
             ['000001', 1],
             ['000999', 999],
             ['000000', 0],
             ['123456789', 123456789],
        ];
    }
}
